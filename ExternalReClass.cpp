#include "prochack.h"

HANDLE hProc;

struct Vector3 { float x, y, z; };

class playerent;

class weapon
{
public:
	char pad_0x0000[0x4]; //0x0000
	__int32 ID; //0x0004
	playerent * owner; //0x0008
	uintptr_t  * guninfo; //0x000C
	int * ammo2; //0x0010
	int * ammo; //0x0014
	int * gunWait;
	int shots;
	int breload;
};

class playerent
{
public:
	DWORD vTable; //0x0
	Vector3 head; //0x0004
	char _0x0010[36];
	Vector3 pos; //0x0034
	Vector3 angle; //0x0040
	char _0x004C[37];
	BYTE bScoping; //0x0071
	char _0x0072[134];
	__int32 health; //0x00F8
	__int32 armor; //0x00FC
	char _0x0100[292];
	BYTE bAttacking; //0x0224
	char name[16]; //0x0225
	char _0x0235[247];
	BYTE team; //0x032C
	char _0x032D[11];
	BYTE state; //0x0338
	char _0x0339[59];
	weapon* pWeapon; //0x0374
	char _0x0378[520];
};

template <class Z>
class ExtInterface
{
public:
	Z* proxy;
	uintptr_t ext;

	ExtInterface(uintptr_t ext, Z* ExtInterface)
	{
		this->ext = ext;
		this->proxy = ExtInterface;
	}

	template <typename T>
	T Get(T* var)
	{
		T buffer;

		//resolve offset dynamically
		uintptr_t offset = (uintptr_t)var - (uintptr_t)proxy;

		//add offset to get to member variable
		uintptr_t extAddr = ext + offset;

		//Read it
		ReadProcessMemory(hProc, (void*)extAddr, &buffer, sizeof(buffer), nullptr);

		return buffer;
	}

	template <typename T>
	void Set(T* var, T value)
	{
		T buffer;
		uintptr_t offset = (uintptr_t)var - (uintptr_t)proxy;
		uintptr_t extAddr = ext + offset;
		WriteProcessMemory(hProc, (void*)extAddr, &value, sizeof(buffer), nullptr);
	}
};

#define GET(obj, var) obj.Get(&obj.proxy->var);
#define SET(obj, var, val) obj.Set(&obj.proxy->var, val);

int main()
{
	DWORD procid;
	GetPid(L"ac_client.exe", &procid);
	hProc = OpenProcess(PROCESS_ALL_ACCESS, NULL, procid);

	//Create 1 local proxy object for all objects of type "playerent" to use
	playerent* proxy_playerent = new playerent;

	//Create interface between proxy and external object
	ExtInterface<playerent> player(EvalPtr(hProc, 0x50f4f4, 0, 0), proxy_playerent);

	auto health = GET(player, health);
	SET(player, health, 1337);

	//When member variable is a ptr to a class object you need to create more interfaces
	auto weaponAddr = (uintptr_t)GET(player, pWeapon);
	ExtInterface<weapon> gun(weaponAddr, new weapon);
	auto ammoAddr = (uintptr_t)GET(gun, ammo);
	auto ammo = EvalPtr(hProc, ammoAddr);

	return 0;
}
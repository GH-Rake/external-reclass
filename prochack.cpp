#pragma once
#include "prochack.h"

bool GetPid(const wchar_t* targetProcess, DWORD* procID)
{
	HANDLE snap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (snap && snap != INVALID_HANDLE_VALUE)
	{
		PROCESSENTRY32 pe;
		pe.dwSize = sizeof(pe);
		if (Process32First(snap, &pe))
		{
			do
			{
				if (!wcscmp(pe.szExeFile, targetProcess))
				{
					CloseHandle(snap);
					*procID = pe.th32ProcessID;
					return true;
				}
			} while (Process32Next(snap, &pe));
		}
	}
	return false;
}

DWORD GetModuleBase(const wchar_t* ModuleName, DWORD procID)
{
	MODULEENTRY32 ModuleEntry = { 0 };
	HANDLE SnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, procID);

	if (!SnapShot) return NULL;

	ModuleEntry.dwSize = sizeof(ModuleEntry);

	if (!Module32First(SnapShot, &ModuleEntry)) return NULL;

	do
	{
		if (!wcscmp(ModuleEntry.szModule, ModuleName))
		{
			CloseHandle(SnapShot);
			return (DWORD)ModuleEntry.modBaseAddr;
		}
	} while (Module32Next(SnapShot, &ModuleEntry));

	CloseHandle(SnapShot);
	return NULL;
}

uintptr_t EvalPtr(HANDLE hProc, uintptr_t ptr)
{
	if (!ReadProcessMemory(hProc, (LPCVOID)ptr, &ptr, sizeof(ptr), nullptr))
	{
		return 0; //if RPM fails
	}
	return ptr;
}

uintptr_t EvalPtr(HANDLE hProc, uintptr_t ptr, size_t numOffsets, uintptr_t offsets[])
{
	//Dereference ptr by reading the value of the ptr
	if (!ReadProcessMemory(hProc, (LPCVOID)ptr, &ptr, sizeof(ptr), nullptr))
	{
		return 0; //if RPM fails
	}

	if (numOffsets == 0)
	{
		return ptr;
	}

	for (size_t i = 0; i < numOffsets; i++)
	{
		//Add offset and dereference that address
		if (!ReadProcessMemory(hProc, (LPVOID)(ptr + offsets[i]), &ptr, sizeof(ptr), nullptr))
		{
			return 0; //if RPM fails
		}
	}

	return ptr;
}
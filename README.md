# External ReClass POC
This was just an idea I had to be able to use ReClass generated classes in external projects.  
My implementation is limited to immediate member variables and does not work on pointers.
Hoping someone will take this idea and make a better one.

### Why?
ReClass generated classes make life so easy in internal projects.  Recording, compiling and using lists of offsets is just not necessary with ReClass being available. 
Why limits it's advantages to just internal?

### Usage
1. Reverse a class using ReClass, generate the header file and include in your project.  
2. Read the comments in the code basic flow is:  
3. Create a local proxy object of the class type you reverse such as "player"  
4. Use ExtInterface template class to create an interface object.  
5. Initialize it with pointers to the local proxy and external object.  
6. Use GET and SET macros to easily read and write member variables.  

### To do:
Make it work with member variables that are pointers or other objects.  
Parsing the generated header files and creating a lookup table of offsets would be a better solution.

### Links

[Related GH Thread](https://guidedhacking.com/showthread.php?9550)  
[Latest version of ReClass](https://github.com/dude719/ReClassEx)
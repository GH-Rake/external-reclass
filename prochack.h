#pragma once
#include <Windows.h>
#include <TlHelp32.h>

bool GetPid(const wchar_t* targetProcess, DWORD* procID);

DWORD GetModuleBase(const wchar_t* ModuleName, DWORD ProcessId);

uintptr_t EvalPtr(HANDLE hProc, uintptr_t ptr);

uintptr_t EvalPtr(HANDLE hProc, uintptr_t ptr, size_t numOffsets, uintptr_t offsets[]);
